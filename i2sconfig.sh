#!/usr/bin/env bash
#-------------------------------------------------------------------------------
# Configuration menu for reading microphone array with I2S interfaces
#	using FPGA MicArrayBoard
# Requires the utility `dialog` installed
# version 1.1
#-------------------------------------------------------------------------------

# __Change those paths according to your needs__
# The USB descriptor source file
DESC="../FX2LP_audio_ISO/descriptor.a51"
# The FPGA source file with constants
KONST="../MicArrayBoard_I2S_to_USB/vhdl/konstanty.vhd"

if [ ! -e $DESC ]; then
	echo "File ${DESC} does not exist"
	exit 1
fi

if [ ! -e $KONST ]; then
	echo "File ${KONST} does not exist"
	exit 1
fi

#-------------------------------------------------------------------------------
# Select number of I2S interfaces
# --menu text height width menu-height
interfaces=$(dialog --stdout --menu "Select number of I2S interfaces" \
	14 42 4 \
	1 "1 interface (2 microphones)" \
	2 "2 interfaces (4 microphones)" \
	4 "4 interfaces (8 microphones)" \
	8 "8 interfaces (16 microphones)");

clear;

# Dialog cancelled
if [ -z $interfaces ]; then
	exit
fi

# Select sampling frequency
f_s=$(dialog --stdout --menu "Select sampling frequency" \
	14 42 4 \
	8 "8 kHz" \
	16 "16 kHz" \
	24 "24 kHz" \
	48 "48 kHz");

clear;

# Dialog cancelled
if [ -z $f_s ]; then
	exit
fi

# Packet size [samples]
# assuming packet interval 125 us
packet=$(echo "$f_s * 0.125 * 2 * $interfaces" | bc)

# convert to integer (use C float format)
packet=$(LC_NUMERIC=C printf "%.0f" $packet)

echo "Packet size: ${packet} samples"

#-------------------------------------------------------------------------------

text="Selected configuration: ${interfaces} I2S interface(s), "
text+="${f_s} kHz sampling frequency, "
text+="packet size ${packet} samples.\n\n"
text+="Apply new configuration? The following files will be modified:\n"
text+="${DESC}\n${KONST}"

dialog --stdout --yesno "${text}" 14 64
# yes returns 0, no returns 1
answer=$?

clear;

if [ $answer -ne 0 ]; then
	# no
	exit 1;
fi

# Find and replace the value of constants for FPGA configuration:
# ROZHRANI, VYBRANA_FVZ and VZORKU_NA_PAKET
sed -i "s#\(constant ROZHRANI.*:=\).*;#\1 ${interfaces};#" $KONST
sed -i "s#\(constant VYBRANA_FVZ.*:=\).*;#\1 fvz_${f_s}k;#" $KONST
sed -i "s#\(constant VZORKU_NA_PAKET.*:=\).*;#\1 ${packet};#" $KONST

channels=$((2*interfaces));

# Find and replace the value of constants for FX2LP descriptor:
# F_SAMP and CHANNELS
sed -i "s/F_SAMP =.*;/F_SAMP = ${f_s}000 ;/" $DESC
sed -i "s/CHANNELS =.*/CHANNELS = ${channels}/" $DESC

echo "Files were modified to apply selected configuration."

