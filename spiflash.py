#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# Script for writing binary data to the SPI flash connected to FPGA
# using the fx2lp USB microcontroller GPIO pins on the FPGA MicArray board
# created: 2023-06-06 - 2023-12-22, version 2.1
#-------------------------------------------------------------------------------
# Copyright (C) 2023 Jan Šedivý
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

import sys
import argparse
# import the libfpgalink library wrapper for python
import fl

# connections to the SPI pins of the flash memory on the FPGA MicArray board
# MISO = PD5, MOSI = PD6, CS = PA0, SCK = PD7
PROG_CONFIG = "D5D6A0D7"

#-------------------------------------------------------------------------------
# SPI FLASH INSTRUCTIONS
# Read Data, 3 bytes address, data
INST_READ_DATA = 0x03
# Page Program, 3 bytes address, data (max 256 bytes)
INST_PAGE_WRITE = 0x02
# Write Enable
INST_WRITE_EN = 0x06
# Sector Erase (4KB), 3 bytes address
INST_SECTOR_ERASE = 0x20
# Chip Erase - erase whole memory (set to 0xFF)
INST_CHIP_ERASE = 0x60
# Read Status Register 1,2
INST_READ_SR1 = 0x05
INST_READ_SR2 = 0x35
# Write Status Register, SR1, SR2
INST_WRITE_SR = 0x01

#-------------------------------------------------------------------------------
# SPI FUNCTIONS

# the fx2lp chip reference, used in the functions
handle = fl.FLHandle()

# get the SPI Slave Select pin - @return tuple (port, bit)
def get_SS():
	(SS_port, SS_bit) = fl.progGetPort(handle, fl.LP_SS)
	fl.flSingleBitPortAccess(handle, SS_port, SS_bit, fl.PIN_HIGH)
	return (SS_port, SS_bit)

# general SPI transfer
# @param send: bytearray of data to send
# @param to_read: number of bytes to read
# @return read bytes array
def spi_transfer(send, to_read):
	read_data = bytes(0)
	(SS_port, SS_bit) = get_SS()
	fl.flSingleBitPortAccess(handle, SS_port, SS_bit, fl.PIN_LOW)
	fl.spiSend(handle, send, fl.SPI_MSBFIRST)
	if to_read > 0:
		read_data = fl.spiRecv(handle, to_read, fl.SPI_MSBFIRST)
	fl.flSingleBitPortAccess(handle, SS_port, SS_bit, fl.PIN_HIGH)
	return read_data

def write_en():
	spi_transfer(bytes([INST_WRITE_EN]), 0)

# write up to 256 bytes at once
def write_page(adr, data):
	SEND = bytearray([INST_PAGE_WRITE])
	SEND.extend(bytes([(adr >> 16) & 0xff, (adr >> 8) & 0xff, adr & 0xff]))
	SEND.extend(bytes(data))
	spi_transfer(SEND, 0)

# read status register 2 & 1, print the values
def read_status():
	sr2 = spi_transfer(bytes([INST_READ_SR2]), 1)[0]
	sr1 = spi_transfer(bytes([INST_READ_SR1]), 1)[0]
	print("SR {:02X} {:02X}".format(sr2, sr1))
	return (sr2, sr1)

# Send READ DATA command, retrieve num bytes back
# @param adr: 3byte internal address
def read(adr, num):
	read_data = bytearray(num)
	SEND = bytearray([INST_READ_DATA])
	SEND.extend(bytes([(adr >> 16) & 0xff, (adr >> 8) & 0xff, adr & 0xff]))
	read_data = spi_transfer(SEND, num)
	return read_data

# erase a 4kB sector located at adr
def sector_erase(adr):
	SEND = bytearray([INST_SECTOR_ERASE])
	SEND.extend(bytes([(adr >> 16) & 0xff, (adr >> 8) & 0xff, adr & 0xff]))
	spi_transfer(SEND, 0)

# wait while the memory is busy (timeout in 10 seconds)
def wait_while_busy(error_msg):
	sr1 = spi_transfer(bytes([INST_READ_SR1]), 1)[0]
	for t in range(10*5):
		sr1 = spi_transfer(bytes([INST_READ_SR1]), 1)[0]
		if not (sr1 & 0x01):
			break
		fl.flSleep(200)# sleep 200 ms
	else:
		raise Exception(error_msg)

# erase whole memory (status timeout)
def chip_erase():
	print("erasing whole flash memory...")
	spi_transfer(bytes([INST_CHIP_ERASE]), 0)
	# wait while chip is busy
	wait_while_busy("erasing took too long")

# disable write protection bits in status register 1
def unprotect():
	(sr2, sr1) = read_status()
	# check the sector, top and block protect bits in SR1
	# and complement protect bit in SR2
	if (sr1 & 0x7C) != 0 or (sr2 & 0x40) != 0:
		print("memory is protected, removing protection...")
	elif (sr2 & 0x38) != 0:
		# OTP protect bits are set
		print("memory is bricked, you need to replace it")
		exit(1)
	else:
		print("memory is not protected")
		return
	# write 0 to all protect bits
	# be careful not to set the one-time programmable bits in SR2 to 1
	write_en()
	# write instruction, SR1, SR2
	spi_transfer(bytes([INST_WRITE_SR, 0x00, 0x00]), 0)

# write binary file to the flash (begin at address 0)
# @param data: byte array of the file content
def write_file(data):
	pages_to_write = len(data)//256 + (0 if len(data) % 256 == 0 else 1)
	print("writing {:d} pages...".format(pages_to_write))
	for p in range(0, pages_to_write):
		page = data[(p*256):(p*256) + 256]
		# enable write before writing to each page
		write_en()
		write_page(p*256, page)
		# wait while chip is busy
		wait_while_busy("writing took too long")

#-------------------------------------------------------------------------------

# reads a binary file located in @param path
# @return binary data array if successful
def readbin(path):
	try:
		file = open(path,"rb")
		data = file.read()
		file.close()
		return data
	except:
		print(f"error reading file {path}")
		return

# get hexadecimal USB identifier value as integer from input parameter
# https://stackoverflow.com/questions/14117415/how-can-i-constrain-a-value-parsed-with-argparse-for-example-restrict-an-integ
def id_hex_int(_hex):
	_int = int(_hex, 16)
	# valid range (two bytes): 0 - 0xffff
	if _int < 0 or _int > 0xffff:
		err = "{:X} is not a valid identifier value".format(_int)
		raise argparse.ArgumentTypeError(err)
	return _int

#-------------------------------------------------------------------------------
# MAIN


description = """Python tool for writing SPI flash memory connected to FPGA
	using the FPGAlink library"""

# VID:PID of the libfpgalink standard firmware (that emulates SPI)
# 1d50:602b OpenMoko, Inc. FPGALink
STD_DEVICE = "1d50:602b"

if __name__ == "__main__":
	exitval = 0

	# parse command-line parameters
	parser = argparse.ArgumentParser(description = description)
	# the .bit file to flash - positional command-line parameter
	parser.add_argument('bitfile', type = str, default = "citac_8bit.bit",
		help = 'the FPGA bit configuration (.bit) file to flash')
	# the initial USB VID:PID of the board
	parser.add_argument('-p', '--pid', type = id_hex_int, required = True,
		help = 'initial USB product ID of the board in hexadecimal format')
	parser.add_argument('-v', '--vid', type = id_hex_int, required = True,
		help = 'initial USB vendor ID of the board in hexadecimal format')
	# optional parameter: verify memory content after flashing
	parser.add_argument('-f', '--verify', action = 'store_true',
		required = False,
		help = 'verify the memory content after writing is finished')

	args = parser.parse_args()
	print(args)

	print("bit file to flash: {}".format(args.bitfile))

	bitdata = readbin(args.bitfile)
	print("file size: {} bytes".format(len(bitdata)))

	VID_PID = "{:04x}:{:04x}".format(args.vid, args.pid)
	print("trying to open USB device with VID: {:04x}; PID: {:04x}"
		.format(args.vid, args.pid))

	try:
		fl.flInitialise(0)

		print("loading standard firmware into device " + VID_PID)
		fl.flLoadStandardFirmware(VID_PID, STD_DEVICE)

		print("waiting for renumeration")
		# wait for renumeration - up to 5s (1s + 40 * 100ms)
		# libusb needs a few seconds to register new device
		fl.flAwaitDevice(STD_DEVICE, 40)

		# open the emulated SPI interface
		handle = fl.flOpen(STD_DEVICE)
		fl.progOpen(handle, PROG_CONFIG)

		unprotect()
		read_status()

		# erase the chip (blocking, waits for status)
		write_en()
		chip_erase()

		print("writing the bit file...")
		# enable write before writing to memory
		write_en()
		write_file(bitdata)
		# wait a bit after writing
		fl.flSleep(10)
		print("writing finished")

		if args.verify:
			print("verifying flash contents...")
			flash_content = read(0, len(bitdata))
			# loop over the received memory content and compare it with original
			for i in range(0, len(bitdata)):
				if flash_content[i] != bitdata[i]:
					print("data mismatch at {} (read: 0x{:02x} expected: 0x{:02x})"
						.format(i, flash_content[i], bitdata[i]))
					exitval = 1
					break
			print("verifying OK")

		# Close SPI interface
		fl.progClose(handle)

	except Exception as ex:
		print(f"ERROR: {ex}")
	finally:
		fl.flClose(handle)

	exit(exitval)

