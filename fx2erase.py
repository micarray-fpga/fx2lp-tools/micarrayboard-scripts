#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# Erase the EEPROM connected to FX2LP USB microcontroller
# This is done by utilizing the fx2loader library
# created: 2023-09-22, version 1.1
#-------------------------------------------------------------------------------
# Copyright (C) 2023 Jan Šedivý
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

import sys
import time
import argparse
import array

# import the libfpgalink library wrapper for python
import fl

# C library binding
from ctypes import *

# bind the fx2loader and usbwrap libraries
fx2loader = CDLL("libfx2loader.so")
usbwrap = CDLL("libusbwrap.so")

#-------------------------------------------------------------------------------
# C library definitions

class USBDeviceSrtuct(Structure):
	pass

USBDevice = POINTER(USBDeviceSrtuct)
ErrorString = c_char_p
SUCCESS = 0

# FX2Status fx2WriteEEPROM( struct USBDevice *device, const uint8 *bufPtr,
#	uint32 numBytes, const char **error)
fx2loader.fx2WriteEEPROM.argtypes = [USBDevice, POINTER(c_ubyte),
	c_uint, POINTER(ErrorString)]

# Writes raw data to I2C EEPROM
# @param data is an array of bytes to write
def fx2WriteEEPROM(device : USBDevice, data : bytearray):
	error = ErrorString()
	data_len = len(data)
	# cast to byte array pointer from char array pointer
	array = cast(c_char_p(bytes(data)), POINTER(c_ubyte))
	status = fx2loader.fx2WriteEEPROM(device, array, data_len, byref(error))
	if (status != SUCCESS):
		s = str(error.value)
		fx2loader.fx2FreeError(error)
		raise Exception(s)

# USBStatus usbInitialise(int debugLevel, const char **error)
usbwrap.usbInitialise.argtypes = [c_int, POINTER(ErrorString)]

def usbInitialise(debugLevel : int):
	error = ErrorString()
	status = usbwrap.usbInitialise(debugLevel, byref(error))
	if (status != SUCCESS):
		s = str(error.value)
		usbwrap.usbFreeError(error)
		raise Exception(s)

# USBStatus usbOpenDevice(const char *vp, int configuration, int iface,
#	int alternateInterface, struct USBDevice **devHandlePtr, const char **error)
usbwrap.usbOpenDevice.argtypes = [c_char_p, c_int, c_int, c_int,
	POINTER(USBDevice), POINTER(ErrorString)]

def usbOpenDevice(VID_PID : str, config : int, iface : int, altIface : int):
	device = USBDevice()
	error = ErrorString()
	status = usbwrap.usbOpenDevice(VID_PID.encode('ascii'), config, iface,
		altIface, byref(device), byref(error))
	if (status != SUCCESS):
		s = str(error.value)
		usbwrap.usbFreeError(error)
		raise Exception(s)
	return device

# void usbCloseDevice(struct USBDevice *dev, int iface)
usbwrap.usbCloseDevice.argtypes = [USBDevice, c_int]
usbwrap.usbCloseDevice.restype = None

# void usbShutdown();
usbwrap.usbShutdown.restype = None

#-------------------------------------------------------------------------------

# get hexadecimal USB identifier value as integer from input parameter
# https://stackoverflow.com/questions/14117415/how-can-i-constrain-a-value-parsed-with-argparse-for-example-restrict-an-integ
def id_hex_int(_hex):
	_int = int(_hex, 16)
	# valid range (two bytes): 0 - 0xffff
	if _int < 0 or _int > 0xffff:
		err = "{:X} is not a valid identifier value".format(_int)
		raise argparse.ArgumentTypeError(err)
	return _int

#-------------------------------------------------------------------------------
# MAIN

fx2 = None

# VID:PID of the libfpgalink standard firmware (second stage loader)
# 1d50:602b OpenMoko, Inc. FPGALink
STD_DEVICE = "1d50:602b"

if __name__ == "__main__":
	ret = 0 # return value
	print("""Python script for erasing FX2LP firmware from the EEPROM memory
	using the libfpgalink library""")

	parser = argparse.ArgumentParser(
		description = 'Erase the I2C EEPROM connected to FX2LP.')
	parser.add_argument('-v', '--vid', type = id_hex_int, required = True,
		help = 'initial USB vendor ID of the FX2LP in hexadecimal format')
	parser.add_argument('-p', '--pid', type = id_hex_int, required = True,
		help = 'initial USB product ID of the FX2LP in hexadecimal format')
	# optional parameters
	parser.add_argument('-s', '--size', type = int, required = False,
		default = 2048,
		help = 'size of the memory block in bytes to erase (default 2048)')
	parser.add_argument('-d', '--debug', action = 'store_true',
		required = False, help = 'print libusb debugging output')

	args = parser.parse_args()
	VID_PID = "{:04x}:{:04x}".format(args.vid, args.pid)

	try:

		## LOAD the second stage loader with libfpgalink
		fl.flInitialise(0)

		print("Loading standard firmware into device " + VID_PID)
		fl.flLoadStandardFirmware(VID_PID, STD_DEVICE)

		print("Waiting for renumeration")
		# wait for renumeration - up to 5s (1s + 40 * 100ms)
		# libusb needs a few seconds to register new device
		fl.flAwaitDevice(STD_DEVICE, 40)

		## ERASE the EEPROM with libfx2loader
		if args.debug:
			usbInitialise(4) # debug output
		else:
			usbInitialise(3)

		print("Opening new device VID:PID " + STD_DEVICE)
		# open the FX2LP chip with loaded firmware
		fx2 = usbOpenDevice(STD_DEVICE, 1, 0, 0)
		# erase block of bytes in the EEPROM
		print(f"Erasing block of {args.size} bytes...")
		fx2WriteEEPROM(fx2, bytearray([0x00] * args.size))
		print("Finished successfully")

	except Exception as ex:
		print(ex)
		ret = 1
	finally:
		if fx2:
			usbwrap.usbCloseDevice(fx2, 0)
		usbwrap.usbShutdown()
		exit(ret)

